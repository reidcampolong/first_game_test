package me.reid.objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.util.Random;

import javax.swing.SwingUtilities;

import me.reid.Game;
import me.reid.game.GameObject;
import me.reid.game.ID;
import me.reid.input.ClickEvent;
import me.reid.input.MouseClick;

public class BasicEnemy extends GameObject {
	
	static Random r = new Random();
	public BasicEnemy(int x, int y, ID id) {
		super(x, y, id);
		
		velX = r.nextInt(5) + 1;
		velY = r.nextInt(5) + 1;
	}
	
	public void tick() {
		//x += velX;
		//y += velY;
		
		// Game Boundaries
		if(y <= 0 || y >= (Game.HEIGHT - 16)) velY *= -1;
		if(x <= 0 || x >= (Game.WIDTH - 16)) velX *= -1;
		
		if(ClickEvent.toggle){
			double mx = MouseClick.mx;
			double my = MouseClick.my;
			double xDistance = (mx - x) + r.nextInt(100);
			double yDistance = (my - y) + r.nextInt(100);
			double distance = Math.sqrt((xDistance * xDistance) + (yDistance * yDistance));
			if (distance > 1) {
				x += xDistance * 0.03;
				y += yDistance * 0.03;
			}
		} else {
			x += velX + r.nextInt(1);
			y += velY + r.nextInt(1);
		}
	}
	
	public void render(Graphics g) {
		g.setColor(Color.RED);
		g.fillRect(x, y, 16, 16);
	}

}
