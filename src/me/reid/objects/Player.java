package me.reid.objects;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import me.reid.Game;
import me.reid.game.GameObject;
import me.reid.game.ID;

public class Player extends GameObject {
	
	Random r = new Random();
	
	public Player(int x, int y, ID id) {
		super(x, y, id);
		
		velX = 5;
		velY = 5;
	}
	
	public void tick() {
		x += velX;
		y += velY;
		
		// Game Boundaries

		x = Game.bounds(x, 0, Game.WIDTH - 35);
		y = Game.bounds(y, 0, Game.HEIGHT - 60);
	}
	
	public void render(Graphics g) {
		g.setColor(Color.WHITE);
		g.fillRect(x, y, 32, 32);
	}

}
