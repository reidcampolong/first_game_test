package me.reid.input;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ClickEvent extends MouseAdapter {
	
	public static boolean toggle;
	public void mouseClicked(MouseEvent e) {
		toggle = !toggle;
	}
}
