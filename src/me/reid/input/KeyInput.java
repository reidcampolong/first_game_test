package me.reid.input;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import me.reid.game.GameObject;
import me.reid.game.Handler;
import me.reid.game.ID;

public class KeyInput extends KeyAdapter {
	
	// 87 W
	// 65 A
	// 83 S
	// 68 D
	
	private Handler handler;
	
	public KeyInput(Handler handler) {
		this.handler = handler;
	}

	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		
		for(GameObject go : handler.object)
			if(go.getID() == ID.PLAYER) {
				if(key == KeyEvent.VK_W) go.setVelY(-5);
				else if(key == KeyEvent.VK_S) go.setVelY(5);
				else if(key == KeyEvent.VK_A) go.setVelX(-5);
				else if(key == KeyEvent.VK_D) go.setVelX(5);
			}
			
	}
	
	public void keyReleased(KeyEvent e) {
		//int key = e.getKeyCode();
		
	}

}
