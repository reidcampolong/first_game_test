package me.reid.game;

import java.awt.Graphics;
import java.util.LinkedList;

public class Handler {

	// Store all the game objects
	public LinkedList<GameObject> object = new LinkedList<GameObject>();
	
	public void tick() {		
		// Tick game objects
		for(int i = 0; i < object.size(); i++) {
			object.get(i).tick();
		}
	}
	
	public void render(Graphics g) {
		// Render game objects
		for(int i = 0; i < object.size(); i++) {
			object.get(i).render(g);
		}
	}
	
	public void addObject(GameObject object) {
		this.object.add(object);
	}
	
	public void removeObject(GameObject object) {
		this.object.remove(object);
	}
}
