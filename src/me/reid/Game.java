package me.reid;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.util.Random;

import me.reid.game.Handler;
import me.reid.game.ID;
import me.reid.input.ClickEvent;
import me.reid.input.KeyInput;
import me.reid.input.MouseClick;
import me.reid.objects.BasicEnemy;

public class Game extends Canvas implements Runnable {
	
	private static final long serialVersionUID = -1665782374751782055L;

	public static final int WIDTH = 500;
	public static final int HEIGHT = 680;
	private static final String TITLE = "Click Test Screen";
	
	private Thread thread;
	private boolean running;
	
	private Handler handler;
	private Window window;
	
	public Game() {
		handler = new Handler();
		this.addKeyListener(new KeyInput(handler));
		this.addMouseMotionListener(new MouseClick());
		this.addMouseListener(new ClickEvent());
		
		window = new Window(this, WIDTH, HEIGHT, TITLE);
		//handler.addObject(new Player((WIDTH/2) - 32, (HEIGHT/2) - 32, ID.PLAYER));
		
		Random r = new Random();
		for(int i = 0; i < 100; i ++)
			handler.addObject(new BasicEnemy(r.nextInt(WIDTH), r.nextInt(HEIGHT), ID.ENEMY_BASIC));
	}
	
	public static void main(String[] args) {
		new Game();
	}
	
	public synchronized void start() {
		thread = new Thread(this);
		thread.start();
		running = true;
	}
	
	public synchronized void stop() {
		try {
			thread.join();
			running = false;
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	public void run() {
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double ns = (1000000000 / amountOfTicks);
		double delta = 0;
		long timer = System.currentTimeMillis();
		int frames = 0;
		
		while(running) {
			long now = System.nanoTime();
			delta += (now-lastTime) / ns;
			lastTime = now;
			while(delta >= 1){
				tick();
				delta--;
			}
			if(running)
				render();
			frames++;
			
			if(System.currentTimeMillis() - timer > 1000) {
				timer += 1000;
				window.frame.setTitle("Reid | FPS: " + frames);
				frames = 0;
			}
		}
		stop();
	}
	
	public static int bounds(int var, int min, int max) {
		if(var >= max)
			return var = max - 1;
		else if(var <= min)
			return var = min + 1;
		else return var;
	}
	
	private void tick(){
		handler.tick();
	}
	
	private void render(){
		BufferStrategy bs = this.getBufferStrategy();
		if(bs == null) {
			this.createBufferStrategy(3);
			return;
		}
		
		Graphics g = bs.getDrawGraphics();
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		handler.render(g);
		
		g.dispose();
		bs.show();
	}

}
