package me.reid;

import java.awt.Canvas;

import javax.swing.JFrame;

public class Window extends Canvas {
	
	private static final long serialVersionUID = 728719783418729830L;
	
	public JFrame frame;
	
	public Window(Game game, int width, int height, String title) {
		frame = new JFrame(title);
		
		frame.setSize(width, height);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		
		frame.add(game);
		frame.setFocusable(true);
		frame.setVisible(true);
		
		game.start();
	}

}
